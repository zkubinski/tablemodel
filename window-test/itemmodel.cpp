#include "itemmodel.h"

ItemModel::ItemModel(QObject *parent) : QAbstractItemModel(parent),
    sizRow(4), sizColumn(4), containerForData(sizRow,QVector<QString>(sizColumn))
{
    for(quint32 i=0; i<containerForData.size(); ++i){
        for(quint32 j=0; j<containerForData[i].size(); ++j){
            containerForData[i][j]=QString("Liczba ")+QString("%1").arg(QString::number(i+1));
        }
    }
}

void ItemModel::selectedRow(const QModelIndex &index)
{
    qInfo()<< "current row" << index.row();
    qInfo()<< "current column" << index.column();
    // qInfo()<< Qt::endl;
    // qInfo()<< "prev row" << right.row();
    // qInfo()<< "prev column" << right.column();
}

QModelIndex ItemModel::index(int row, int column, const QModelIndex &parent) const
{
    return createIndex(row,column,parent.internalPointer());
}

QModelIndex ItemModel::parent(const QModelIndex &child) const
{
    Q_UNUSED(child);

    return QModelIndex();
}

int ItemModel::rowCount(const QModelIndex &parent) const
{
    return containerForData.size();
}

int ItemModel::columnCount(const QModelIndex &parent) const
{
    return containerForData[0].size();
}

QVariant ItemModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid()){
        return QVariant();
    }
    if(role==Qt::DisplayRole || role == Qt::EditRole){
        QVariant insertData;
        insertData = containerForData[index.row()][index.column()];
        return insertData;
    }

    return QVariant();
}

bool ItemModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(role==Qt::EditRole){
        containerForData[index.row()][index.column()] = value.toString();

        emit dataChanged(index,index,QList<int>(Qt::DisplayRole));

        return true;
    }
    return false;
}

Qt::ItemFlags ItemModel::flags(const QModelIndex &index) const
{
    if(!index.isValid()){
        return Qt::NoItemFlags;
    }
    return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
}

QVariant ItemModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(orientation == Qt::Vertical && role == Qt::DisplayRole){
        return QVariant(QString("%1").arg(section+1));
    }
    if(orientation == Qt::Horizontal && role == Qt::DisplayRole){
        return QVariant(QString("Liczba %1").arg(section+1));
    }
    return QVariant();
}

bool ItemModel::insertRows(int row, int count, const QModelIndex &parent)
{
    int fromStartRow = (containerForData.size()+1);
    int toEndRow = (fromStartRow);

    beginInsertRows(parent, fromStartRow, toEndRow);

    containerForData.push_back(QVector<QString>(sizeColumn()));

    endInsertRows();

    return true;
}

bool ItemModel::insertColumns(int column, int count, const QModelIndex &parent)
{
    int fromStartColumn = (containerForData[0].size()+1);
    int toEndColumn = (fromStartColumn);

    setSizeColumn(toEndColumn);

    beginInsertColumns(parent, fromStartColumn, toEndColumn);

    for(int i=0; i<containerForData.size(); ++i){
        for(int j=0; j<containerForData[i].size(); ++j){
            containerForData[i].resize(toEndColumn);
            }
        }

    endInsertColumns();

    return true;
}

void ItemModel::removeRowOfDataInModel(QVector<QVector<QString>> &dataFromContainer, int rowToRemove)
{
    QVector<QVector<QString>> tmp;

    for(int i=0; i<dataFromContainer.size(); ++i){
        if(i!=rowToRemove){
            tmp.push_back(dataFromContainer[i]);
        }
    }

    dataFromContainer.clear();
    dataFromContainer=tmp;
}

void ItemModel::removeColumnOfDataInModel(QVector<QVector<QString>> &dataFromContainer, int columnToRemove)
{
    QVector<QVector<QString>> tmp;

    for(int i=0; i<dataFromContainer.size(); ++i){
        tmp.push_back(QVector<QString>(0));//tworzę taką samą ilość wierszy jak w macierzy źródłowej

        for(int j=0; j<dataFromContainer[i].size(); ++j){
            if(j!=columnToRemove){//szukam numeru kolumny którą chcę pominąć przy przepisywaniu do nowej macierzy - tj. usuwam kolumnę
                tmp[i].push_back(dataFromContainer[i][j]);//na nowo przepisuję dane pomijając znalezioną kolumnę
            }
        }
    }

    dataFromContainer.clear();//usuwam stare dane
    dataFromContainer = tmp;//nowa macierz po usunięciu kolumny
}

bool ItemModel::removeRows(int row, int count, const QModelIndex &parent)
{
    int fromStartRow = row;
    int toEndRow = fromStartRow;

    beginRemoveRows(parent, fromStartRow, toEndRow);

        removeRowOfDataInModel(containerForData,row);

    endRemoveRows();

    return true;
}

bool ItemModel::removeColumns(int column, int count, const QModelIndex &parent)
{
    int fromStartColumn = column;
    int toEndColumn = fromStartColumn;

    beginRemoveColumns(parent, fromStartColumn, toEndColumn);

        removeColumnOfDataInModel(containerForData,column);

    endRemoveColumns();

    return true;
}
