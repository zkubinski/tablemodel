#ifndef ITEMMODEL_H
#define ITEMMODEL_H

#include <QAbstractItemModel>
#include <QObject>
#include <QVariant>
#include <QVector>

class ItemModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    explicit ItemModel(QObject *parent=nullptr);

    // QAbstractItemModel interface
    virtual QModelIndex index(int row, int column, const QModelIndex &parent) const override;
    virtual QModelIndex parent(const QModelIndex &child) const override;
    virtual int rowCount(const QModelIndex &parent) const override;
    virtual int columnCount(const QModelIndex &parent) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role) override;
    virtual Qt::ItemFlags flags(const QModelIndex &index) const override;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
    virtual bool insertRows(int row, int count, const QModelIndex &parent) override;
    virtual bool insertColumns(int column, int count, const QModelIndex &parent) override;
    virtual bool removeRows(int row, int count, const QModelIndex &parent) override;
    virtual bool removeColumns(int column, int count, const QModelIndex &parent) override;

    inline int sizeRow(){
        return sizRow;
    }

    inline int sizeColumn(){
        return sizColumn;
    }

    inline void setSizeColumn(int column){
        sizColumn=column;
    }

public slots:
    void selectedRow(const QModelIndex &index);

private:
    int sizRow=0, sizColumn=0;
    QVector<QVector<QString>> containerForData; //kontener macierzy 2d obsługiwany przez model danych - model zapisuje tutaj swoje dane

    void removeRowOfDataInModel(QVector<QVector<QString>> &dataFromContainer, int rowToRemove);
    void removeColumnOfDataInModel(QVector<QVector<QString>> &dataFromContainer, int columnToRemove);
};



#endif // ITEMMODEL_H
