#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    this->resize(800,600);
    mainWidget = new QWidget(this);
    vLayout = new QVBoxLayout(mainWidget);

    myView = new MyTableView(this);
    // view = new QTableView(this);
    myModel = new ItemModel(this);
    myDelegate = new Delegate(this);

    bttLayout = new QHBoxLayout();
    // bttAddRow = new QPushButton(QString(tr("Add row")),this);
    // bttRemoveRow = new QPushButton(QString(tr("Remove row")),this);

    // bttAddColumn = new QPushButton(QString(tr("Add column")),this);
    // bttRemoveColumn = new QPushButton(QString(tr("Remove column")),this);

    bttClose = new QPushButton(QString(tr("Close")),this);
    bttClose->setIcon(QIcon(QPixmap(":icons/exit.png")));

    myView->setModel(myModel);
    myView->setItemDelegate(myDelegate);
    selectionModel = myView->selectionModel();

    // view->setModel(myModel);
    // view->setItemDelegate(myDelegate);
    // selectionModel = view->selectionModel();

    // vLayout->addWidget(view);
    vLayout->addWidget(myView);
    vLayout->addLayout(bttLayout);

    // bttLayout->addWidget(bttAddRow);
    // bttLayout->addWidget(bttRemoveRow);
    // bttLayout->addWidget(bttAddColumn);
    // bttLayout->addWidget(bttRemoveColumn);
    bttLayout->addWidget(bttClose);

    this->setCentralWidget(mainWidget);

    // QObject::connect(view, &QTableView::clicked, this, &MainWindow::rangeOfSelectedCells);
    QObject::connect(myView, &MyTableView::clicked, this, &MainWindow::rangeOfSelectedCells);
    QObject::connect(myView, &MyTableView::clicked, myModel, &ItemModel::selectedRow);
    // QObject::connect(bttAddRow, &QPushButton::clicked, this, [&](){myModel->insertRow(1,QModelIndex());});
    // QObject::connect(bttRemoveRow, &QPushButton::clicked, this, [&](){myModel->removeRow(right.row(),QModelIndex());});

    QObject::connect(bttClose, &QPushButton::clicked, this, &QMainWindow::close);
}

void MainWindow::rangeOfSelectedCells(const QModelIndex &index)
{
    QModelIndex left, right;
    left = myModel->index(index.row(),0,QModelIndex());
    right = myModel->index(index.row(),(myModel->sizeColumn()-1),QModelIndex());

    QItemSelection selection;
    selection.select(left,right);
    selectionModel->select(selection, QItemSelectionModel::Select);
}

MainWindow::~MainWindow()
{
    delete mainWidget;
}
