#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
// #include <QTableView>
#include <QModelIndex>
#include <QItemSelectionModel>
#include <QItemSelection>
#include <QPushButton>

#include "mytableview.h"
#include "delegate.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    QWidget *mainWidget;
    QVBoxLayout *vLayout;
    // QTableView *view;
    MyTableView *myView;
    ItemModel *myModel;
    Delegate *myDelegate;
    QItemSelectionModel *selectionModel;
    QHBoxLayout *bttLayout;
    QPushButton *bttClose/*, *bttAddRow, *bttRemoveRow, *bttAddColumn, *bttRemoveColumn*/;

    void rangeOfSelectedCells(const QModelIndex &);
};
#endif // MAINWINDOW_H
