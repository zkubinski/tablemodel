#include "mytableview.h"

MyTableView::MyTableView(QWidget *parent) : QTableView(parent)
{
    QHeaderView *horizontalHead = this->horizontalHeader();
    QHeaderView *verticalHead = this->verticalHeader();

    setContextMenuPolicy(Qt::CustomContextMenu);

    QObject::connect(horizontalHead, &QHeaderView::sectionClicked, this, &MyTableView::setRemoveColumn);

    // QObject::connect(horizontalHead, &QHeaderView::sectionClicked, this, [&](int horizSection){
        // qInfo()<< "wybrana kolumna" << horizSection+1;
        // QItemSelectionModel *select = this->selectionModel();
        // QItemSelection selection(this->model()->index(0,horizSection), this->model()->index(this->model()->rowCount()-1, horizSection));

        // select->select(selection, QItemSelectionModel::Select);

        // QModelIndexList selectedCells = select->selectedIndexes();

        // for(const QModelIndex &index : selectedCells){
        //     qInfo()<< "row " << index.row()+1 << "columns " << index.column()+1;
        // }
    // });
    QObject::connect(verticalHead, &QHeaderView::sectionClicked, this, &MyTableView::setRemoveRow);

    QObject::connect(this, &QTableView::customContextMenuRequested, this, &MyTableView::showContextMenu);
}

void MyTableView::showContextMenu(const QPoint &pos)
{
    QMenu menu;

    QMenu subMenuInsert;
    subMenuInsert.setTitle(QString("Insert..."));
    subMenuInsert.setIcon(QIcon(QPixmap(":icons/add.png")));

    QAction *actionAddRow = subMenuInsert.addAction(QString("Row"));
    actionAddRow->setIcon(QIcon(QPixmap(":icons/row-add2.png")));

    QAction *actionAddColumn = subMenuInsert.addAction(QString("Column"));
    actionAddColumn->setIcon(QIcon(QPixmap(":icons/column-add2.png")));

    QMenu subMenuRemove;
    subMenuRemove.setTitle(QString("Remove..."));
    subMenuRemove.setIcon(QIcon(QPixmap(":icons/remove.png")));

    QAction *actionRemoveRow = subMenuRemove.addAction(QString("Selected Row"));
    QAction *actionRemoveColumn = subMenuRemove.addAction(QString("Selected Column"));

    menu.addMenu(&subMenuInsert);
    menu.addMenu(&subMenuRemove);

    // QMenu contextMenu;
    // QAction *actionAddRow = contextMenu.addAction(QString(tr("Dodaj wiersz")));
    // QAction *actionRemoveRow = contextMenu.addAction(QString(tr("Usuń wiersz")));

    QObject::connect(actionAddRow, &QAction::triggered, this, &MyTableView::addRow);
    QObject::connect(actionRemoveRow, &QAction::triggered, this, &MyTableView::selectedRowToDelete);

    QObject::connect(actionAddColumn, &QAction::triggered, this, &MyTableView::addColumn);
    QObject::connect(actionRemoveColumn, &QAction::triggered, this, &MyTableView::selectedColumnToDelete);

    // contextMenu->exec(mapToGlobal(pos));
    menu.exec(mapToGlobal(pos));
}

void MyTableView::addRow()
{
    ItemModel *model = qobject_cast<ItemModel*>(this->model()); //"this->model()" sprawdza czy istnieje instancja ustawionego modelu
    //dla tego widoku, a w zasadzie zwraca adres instancji ustawionego modelu, a na jakiej podstawie?
    //ano na takiej, że w klasie "mainwindow.cpp" jest linijka "myView->setModel(myModel);" to tutaj ustawia się model,
    //jak model się ustawi, to klasa bazowa "QAbstractItemView" dla "QTableView" ma metodę "model()", która zwraca za pomocą wskaźnika adres
    //modelu jaki został ustawiony i ot cała filozofia, a potem wiedząc, że adres tego modelu jest klasy "ItemModel" to zapisuję go do tego
    //wskaźnika "ItemModel *model" i dalej to już zwykłe sprawdzenie, czy do tego wskaźnika został przypisany adres - jeżeli tak, to
    //wstawiamy wiersz

    if(model){
        model->insertRows(1,1,QModelIndex());
    }
}

void MyTableView::setRemoveRow(int rowToRemove)
{
    row=rowToRemove;
}

int MyTableView::removeRow()
{
    return row;
}

void MyTableView::selectedRowToDelete()
{
    ItemModel *model = qobject_cast<ItemModel*>(this->model());

    if(this->removeRow()!=-1){
        if(model){
            model->removeRow(this->removeRow(),QModelIndex());
        }
        setRemoveRow(-1);
    }
}
//***************************************************************************************
void MyTableView::addColumn()
{
    ItemModel *model = qobject_cast<ItemModel*>(this->model());

    if(model){
        model->insertColumns(1,1,QModelIndex());
    }
}

void MyTableView::setRemoveColumn(int columnToRemove)
{
    column=columnToRemove;
}

int MyTableView::removeColumn()
{
    return column;
}

void MyTableView::selectedColumnToDelete()
{
    ItemModel *model = qobject_cast<ItemModel*>(this->model());

    if(this->removeColumn()!=-1){
        if(model){
            model->removeColumn(this->removeColumn(),QModelIndex());
        }
        setRemoveColumn(-1);
    }
}

MyTableView::~MyTableView()
{

}
