#ifndef MYTABLEVIEW_H
#define MYTABLEVIEW_H

#include <QWidget>
#include <QTableView>
#include <QMenu>
#include <QAction>
#include <QContextMenuEvent>
#include <QHeaderView>

#include "itemmodel.h"

#include <QItemSelectionModel>
#include <QItemSelection>
#include <QModelIndexList>

class MyTableView : public QTableView
{
    Q_OBJECT
public:
    explicit MyTableView(QWidget *parent=nullptr);
    ~MyTableView();

private slots:
    void showContextMenu(const QPoint &);

    void addRow();
    void setRemoveRow(int rowToRemove);
    int removeRow();
    void selectedRowToDelete();

    void addColumn();
    void setRemoveColumn(int columnToRemove);
    int removeColumn();
    void selectedColumnToDelete();

private:
    int row=-1;
    int column=-1;
};

#endif // MYTABLEVIEW_H
